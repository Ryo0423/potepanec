class Potepan::SuggestController < ApplicationController
  MAX_ITEMS_IN_SUGGEST = 5

  def index
    request = get_suggest(params[:keyword], MAX_ITEMS_IN_SUGGEST)
    if request.status == 200
      render json: request.body, status: request.status
    else
      render json: [], status: request.status
    end
  end

  private

  def get_suggest(keyword, max_num)
    url = ENV['API_URL']
    header = { Authorization: "Bearer #{ENV['API_KEY']}" }
    query = { keyword: keyword, max_num: max_num }

    client = HTTPClient.new
    client.get(url, header: header, query: query)
  end
end
