module Spree::ProductDecorator
  DEFAULT_NUMBER_OF_ACQUISITIONS = 4

  def related_products(number_of_products: DEFAULT_NUMBER_OF_ACQUISITIONS)
    Spree::Product.in_taxons(taxons).
      includes(master: [:default_price, :images]).
      where.not(id: id).distinct.take(number_of_products)
  end

  Spree::Product.prepend self
end
