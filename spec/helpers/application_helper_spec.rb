require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    it "引数にタイトルが渡された時はpage_title - base_titleが返ってくること" do
      expect(helper.full_title(page_title: "test")).to eq "test - BIGBAG Store"
    end

    it "引数がない場合はbase_titleが返ってくること" do
      expect(helper.full_title).to eq "BIGBAG Store"
    end

    it "引数がnilの場合はbase_titleが返ってくること" do
      expect(helper.full_title(page_title: nil)).to eq "BIGBAG Store"
    end
  end
end
