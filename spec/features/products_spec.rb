require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given!(:category) { create(:taxon, name: "Categories") }
  given!(:brand) { create(:taxon, name: "Brands") }
  given!(:lonely) { create(:taxon, name: "Lonelyness") }
  given!(:product) { create(:base_product, taxons: [category, brand]) }
  given!(:product_2) { create(:base_product, taxons: [category, brand]) }
  given!(:product_3) { create(:base_product, taxons: [category, brand]) }
  given!(:product_4) { create(:base_product, taxons: [category, brand]) }
  given!(:product_5) { create(:base_product, taxons: [category, brand]) }
  given!(:product_6) { create(:base_product, taxons: [category, brand]) }
  given!(:lonely_product) { create(:base_product, taxons: [lonely]) }

  scenario "商品詳細ページからトップページへ移動する" do
    # 商品詳細ページ
    visit potepan_product_path(product.id)
    expect(title).to have_content "#{product.name} - BIGBAG Store"
    expect(page).to have_content "As seen on TV!"
    expect(page).to have_content "19.99"

    # トップページに移動
    click_link "Home", match: :first
    expect(page).to have_current_path "/potepan"

    # 商品詳細ページの<title>にはその商品の名前が記載されている
    visit potepan_product_path(product_2.id)
    expect(title).to have_content "#{product_2.name} - BIGBAG Store"
    expect(title).not_to have_content "#{product.name} - BIGBAG Store"
  end

  scenario "商品詳細ページに関連する商品がデフォルトで４つ表示されること" do
    visit potepan_product_path(product.id)
    expect(page).to have_link product_2.name
    expect(page).to have_link product_3.name
    expect(page).to have_link product_4.name
    expect(page).to have_link product_5.name
    expect(page).not_to have_link product_6.name
  end

  scenario "関連しない商品は表示されないこと" do
    visit potepan_product_path(lonely_product.id)
    expect(page).not_to have_link product.name
  end

  scenario "関連商品から商品詳細ページに移動する" do
    visit potepan_product_path(product.id)
    click_on product_2.name
    expect(title).to have_content "#{product_2.name} - BIGBAG Store"
    # 関連する商品が最大４つ表示される
    expect(page).to have_link product.name
    expect(page).to have_link product_3.name
    expect(page).to have_link product_4.name
    expect(page).to have_link product_5.name
    expect(page).not_to have_link product_6.name
  end
end
