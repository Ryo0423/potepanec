require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given!(:categories) { create(:taxonomy, name: "Categories") }
  given!(:bags) { categories.root.children.create(name: "Bags") }
  given!(:snakes) { categories.root.children.create(name: "Snakes") }
  given!(:small_bag) { create(:product, name: "Small bag", taxons: [bags]) }
  given!(:big_bag) { create(:product, name: "Big bag", taxons: [bags]) }
  given!(:big_boss) { create(:product, name: "Big Boss", taxons: [snakes]) }

  scenario "カテゴリーページから製品ページへ移動する" do
    # カテゴリーページに移動
    visit potepan_category_path(Spree::Taxon.first.id)
    expect(title).to have_content "Categories - BIGBAG Store"
    expect(page).to have_selector ".page-title", text: "Categories"
    expect(page).to have_content "Small bag"
    expect(page).to have_content "Big bag"
    expect(page).to have_content "Big Boss"
    # Big Bossの詳細ページに移動
    click_on "Big Boss"
    expect(title).to have_content "Big Boss - BIGBAG Store"
    expect(page).to have_selector ".page-title", text: "Big Boss"
    expect(page).to have_link "一覧ページへ戻る"
  end

  scenario "「一覧ページへ戻る」を押すと、カテゴリーページに戻る" do
    visit potepan_product_path(big_boss.id)
    expect(title).to have_content "Big Boss - BIGBAG Store"
    click_on "一覧ページへ戻る"
    expect(title).to have_content "Snakes - BIGBAG Store"
  end

  scenario "パネルリンクからカテゴリーページに移動する" do
    visit potepan_category_path(Spree::Taxon.first.id)
    expect(page).to have_link "Bags (2)"
    click_on "Bags (2)"
    expect(title).to have_content "Bags - BIGBAG Store"
    expect(page).to have_content "Small bag"
    expect(page).to have_content "Big bag"
    expect(page).not_to have_content "Big Boss"
  end
end
