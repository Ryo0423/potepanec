require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:base_product) { create(:base_product) }

  describe "#products_show" do
    before do
      get :show, params: { id: base_product.id }
    end

    context "商品が存在するとき" do
      it "正常にレスポンスを返すこと" do
        expect(response).to have_http_status "200"
      end

      it "showページがレンダリングされること" do
        expect(response).to render_template("products/show")
      end

      it "インスタンス変数に正しい値が入っていること" do
        expect(assigns(:product)).to eq base_product
      end
    end
  end
end
