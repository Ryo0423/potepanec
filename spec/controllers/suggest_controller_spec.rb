require 'rails_helper'
require 'webmock/rspec'
WebMock.allow_net_connect!(net_http_connect_on_start: true)

RSpec.describe Potepan::SuggestController, type: :controller do
  describe '#index' do
    context "サジェスト機能が成功する場合" do
      before do
        stub_request(
          :get, "https://presite-potepanec-task5.herokuapp.com/potepan/api/suggests?keyword=m&max_num=5"
        ).to_return(
          status: 200, body: ["mario", "mario mini", "mario super"]
        )
        get :index, params: { keyword: "m" }
      end

      it "200レスポンスを返すこと" do
        expect(response).to have_http_status "200"
      end

      it "JSON形式で「m」から始まるワードを返すこと" do
        expect(response.body).to eq "[\"mario\",\"mario mini\",\"mario super\"]"
      end
    end

    context "サジェスト機能が失敗する場合" do
      before do
        stub_request(
          :get, "https://presite-potepanec-task5.herokuapp.com/potepan/api/suggests?keyword=m&max_num=5"
        ).to_return(
          status: 401, body: ["mario", "mario mini", "mario super"]
        )
        get :index, params: { keyword: "m" }
      end

      it "401レスポンスを返すこと" do
        expect(response).to have_http_status "401"
      end

      it "空の配列を返すこと" do
        expect(response.body).to eq "[]"
      end
    end
  end
end
