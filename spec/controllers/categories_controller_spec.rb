require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let!(:taxonomies) { create(:taxonomy, name: "Categories") }
  let!(:taxon) { taxonomies.root.children.create(name: "Rails") }
  let!(:product) { create(:base_product, description: "Test for tying!", taxons: [taxon]) }
  let!(:not_tied) { create(:base_product) }

  describe "categories_show" do
    before do
      get :show, params: { id: taxon.id }
    end

    context "カテゴリーが存在するとき" do
      it "正常にレスポンスを返すこと" do
        expect(response).to have_http_status "200"
      end

      it "showページがレンダリングされること" do
        expect(response).to render_template(:show)
      end

      it "@taxonomiesはSpree::Taxonの全ルートノードであること" do
        expect(assigns(:taxonomies)).to eq Spree::Taxon.roots
      end

      it "@taxonに正しい値が入っていること" do
        expect(assigns(:taxon)).to eq taxon
      end

      it "@productsに正しい値が入っていること" do
        expect(assigns(:products)).to eq taxon.all_products
      end
    end

    context "カテゴリーが商品と紐づいているとき" do
      it "商品が取得できること" do
        expect(taxon.products.first.description).to eq "Test for tying!"
      end
    end

    context "カテゴリーと商品が紐づいていないとき" do
      it "商品が取得できないこと" do
        expect(taxon.products.second).not_to eq not_tied
      end
    end
  end
end
